package me.icyrelic.com.Commands;

import java.util.Date;
import java.util.UUID;

import me.icyrelic.com.AxiomProfile;
import me.icyrelic.com.PUUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;

public class ProfileCommand implements CommandExecutor {
	
	AxiomProfile plugin;
	public ProfileCommand(AxiomProfile instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		 String noPerm = (ChatColor.RED + "You Dont Have Permission!");

		if (cmd.getName().equalsIgnoreCase("profile")) {
			
				
					if (args.length == 1){
			        	 if (args[0].equalsIgnoreCase("newestplayer")){
			        		 if(sender.hasPermission("AxiomProfile.Info.NewestPlayer")){
									sender.sendMessage(plugin.prefix + ChatColor.GREEN+"Newest Player: "+ChatColor.GRAY+plugin.mysql.newestPlayer());
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        	 }
			        	 else if (args[0].equalsIgnoreCase("totalplayers")){
			        		 if(sender.hasPermission("AxiomProfile.Info.TotalPlayers")){
									sender.sendMessage(plugin.prefix + ChatColor.GREEN+"Total Players: "+ChatColor.GRAY+plugin.mysql.totalPlayers());
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        	 }else{
			        		 
			        		 
			        		 if(sender.hasPermission("AxiomProfile.Info.Others")){
			        			 
			        			 String name = args[0].toLowerCase();
			        			 
			        			 if(!PUUID.UUIDExists(name)){
			        				 sender.sendMessage(ChatColor.GRAY + "--------------- "+ChatColor.GREEN+"Player Information"+ChatColor.GRAY+" ---------------");
									 sender.sendMessage(ChatColor.GREEN+"Player Name: " +ChatColor.GRAY+ name);
									 sender.sendMessage(ChatColor.RED + "No Other Information Available For This User");
									 sender.sendMessage(ChatColor.GRAY+"-----------------------------------------------");
			        				 return true;
			        			 }
			        			 
			        			 String suid = PUUID.getUUID(name);
			        			 UUID uid = UUID.fromString(suid);
			        			 
			        			 
			        			 Player p = (Player) plugin.getServer().getPlayer(uid);
			        			 
			        			 
									sender.sendMessage(ChatColor.GRAY + "--------------- "+ChatColor.GREEN+"Player Information"+ChatColor.GRAY+" ---------------");
								sender.sendMessage(ChatColor.GREEN+"Player Name: " +ChatColor.GRAY+ name);
								if(plugin.mysql.checkNewPlayer(uid, name)){
									sender.sendMessage(ChatColor.RED + "No Other Information Available For This User");
								}else{
									
									sender.sendMessage(ChatColor.GREEN+"IP Address: " +ChatColor.GRAY+ plugin.mysql.getIP(uid));
									sender.sendMessage(ChatColor.GREEN+"Player Number: " +ChatColor.GRAY+ plugin.mysql.getPlayerNumber(uid));
									sender.sendMessage(ChatColor.GREEN+"UUID: " +ChatColor.GRAY+ plugin.mysql.getUID(name));
									
									sender.sendMessage(ChatColor.GREEN+"Forum Account");
									sender.sendMessage(ChatColor.GREEN+"- Registered: " +ChatColor.GRAY+ plugin.getAxiomForum().api.hasAccount(name.toLowerCase()));
									sender.sendMessage(ChatColor.GREEN+"- Username: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getUsername(name.toLowerCase()));
									sender.sendMessage(ChatColor.GREEN+"- Email: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getEmail(name.toLowerCase()));
									sender.sendMessage(ChatColor.GREEN+"- Skype: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getSkype(name.toLowerCase()));
									
									
									sender.sendMessage(ChatColor.GREEN+"Server Information");
									sender.sendMessage(ChatColor.GREEN+"- First Seen: " +ChatColor.GRAY+ plugin.mysql.getDate(uid) + " at " + plugin.mysql.getTime(uid));
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"- Last Seen: " +ChatColor.GRAY+ "Online Now!");
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Last Seen: " +ChatColor.GRAY+ plugin.mysql.lastDate(uid) + " at " + plugin.mysql.lastTime(uid));
									}
									
									String days = " Days ";
									String hours = " Hours ";
									String minutes = " Minutes ";
									String seconds = " Seconds ";
									
									
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"- Time Played: " +ChatColor.GRAY+ plugin.mysql.getExactPlayed(uid, days, hours, minutes, seconds, plugin.logins.get(uid),new Date()));
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Time Played: " +ChatColor.GRAY+ plugin.mysql.getPlayed(uid, days, hours, minutes, seconds));
									}
									
									
									sender.sendMessage(ChatColor.GREEN+"Player Information");
									
									
									
									//health
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										Damageable d = (Damageable) p;
										sender.sendMessage(ChatColor.GREEN+"- Health: " +ChatColor.GRAY+ d.getHealth()+"/20");
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Health: " +ChatColor.GRAY+ plugin.mysql.health(uid));
									}
									//hunger
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"- Hunger: " +ChatColor.GRAY+ p.getFoodLevel()+"/20");
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Hunger: " +ChatColor.GRAY+ plugin.mysql.hunger(uid));
									}
									//location
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"- Location: " +ChatColor.GRAY+ "X: "+p.getLocation().getBlockX()+", Y: "+p.getLocation().getBlockY()+", Z: "+p.getLocation().getBlockZ());
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Location: " +ChatColor.GRAY+ plugin.mysql.location(uid));
									}
									//item in hand
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"- Item In Hand: " +ChatColor.GRAY+ p.getItemInHand().getType());
									}else{
										sender.sendMessage(ChatColor.GREEN+"- Item In Hand: " +ChatColor.GRAY+ plugin.mysql.itemInHand(uid));
									}
									
									
									
									
									
									
								}
								sender.sendMessage(ChatColor.GRAY+"-----------------------------------------------");
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        		 
								
			        	 }
					}else{
						
						if(sender instanceof Player){
							if(sender.hasPermission("AxiomProfile.Info.Own")){
							Player p = (Player) sender;
							UUID uid = p.getUniqueId();
							sender.sendMessage(ChatColor.GRAY + "--------------- "+ChatColor.GREEN+"Player Information"+ChatColor.GRAY+" ---------------");
							sender.sendMessage(ChatColor.GREEN+"Player Name: " +ChatColor.GRAY+ sender.getName().toLowerCase());
							sender.sendMessage(ChatColor.GREEN+"IP Address: " +ChatColor.GRAY+ plugin.mysql.getIP(uid));
							sender.sendMessage(ChatColor.GREEN+"Player Number: " +ChatColor.GRAY+ plugin.mysql.getPlayerNumber(uid));
							sender.sendMessage(ChatColor.GREEN+"UUID: " +ChatColor.GRAY+ uid);
							
							sender.sendMessage(ChatColor.GREEN+"Fourm Account");
							sender.sendMessage(ChatColor.GREEN+"- Registered: " +ChatColor.GRAY+ plugin.getAxiomForum().api.hasAccount(p.getName().toLowerCase()));
							sender.sendMessage(ChatColor.GREEN+"- Username: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getUsername(p.getName().toLowerCase()));
							sender.sendMessage(ChatColor.GREEN+"- Email: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getEmail(p.getName().toLowerCase()));
							sender.sendMessage(ChatColor.GREEN+"- Skype: " +ChatColor.GRAY+ plugin.getAxiomForum().api.getSkype(p.getName().toLowerCase()));
							
							sender.sendMessage(ChatColor.GREEN+"Server Information");
							
							
							sender.sendMessage(ChatColor.GREEN+"- First Seen: " +ChatColor.GRAY+ plugin.mysql.getDate(uid) + " at " + plugin.mysql.getTime(uid));
							if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
								sender.sendMessage(ChatColor.GREEN+"- Last Seen: " +ChatColor.GRAY+ "Online Now!");
							}else{
								sender.sendMessage(ChatColor.GREEN+"- Last Seen: " +ChatColor.GRAY+ plugin.mysql.lastDate(uid) + " at " + plugin.mysql.lastTime(uid));
							}
							
							
							String days = " Days ";
							String hours = " Hours ";
							String minutes = " Minutes ";
							String seconds = " Seconds ";
							
							sender.sendMessage(ChatColor.GREEN+"- Time Played: " +ChatColor.GRAY+ plugin.mysql.getExactPlayed(uid, days, hours, minutes, seconds, plugin.logins.get(uid),new Date()));
							
							
							sender.sendMessage(ChatColor.GREEN+"Player Information");
							
							Damageable d = (Damageable) p;
							sender.sendMessage(ChatColor.GREEN+"- Health: " +ChatColor.GRAY+ d.getHealth()+"/20");
							sender.sendMessage(ChatColor.GREEN+"- Hunger: " +ChatColor.GRAY+ p.getFoodLevel()+"/20");
							sender.sendMessage(ChatColor.GREEN+"- Location: " +ChatColor.GRAY+ "X: "+p.getLocation().getBlockX()+", Y: "+p.getLocation().getBlockY()+", Z: "+p.getLocation().getBlockZ());
							sender.sendMessage(ChatColor.GREEN+"- Item In Hand: " +ChatColor.GRAY+ p.getItemInHand().getType());
							
							
							
							
							sender.sendMessage(ChatColor.GRAY+"-----------------------------------------------");
							}else{
								sender.sendMessage(noPerm);
							}
						}else{
							sender.sendMessage(plugin.prefix + ChatColor.RED + "This command is for players only");
						}
						

						
					}
			
		}
		return true;
	}
	

}
