package me.icyrelic.com.Listeners;


import java.util.Date;

import me.icyrelic.com.AxiomProfile;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class ProJoin implements Listener{
	
	AxiomProfile plugin;
	public ProJoin(AxiomProfile instance) {

		plugin = instance;

		}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){

		Player p = event.getPlayer();
		
		if(newPlayer(p)){
			addPlayer(p, getTotalPlayers());
		}
		
		addMySQLPlayer(p);
		
		

	  }
	
	
	public boolean newPlayer(Player p){
		
			return plugin.mysql.checkNewPlayer(p.getUniqueId(), p.getName());
	}
	
	public void addPlayer(Player p, int x){
			plugin.mysql.addPlayer(p, x);
	}
	
	public int getTotalPlayers(){
			return plugin.mysql.totalPlayers();
	}
	
	public void addMySQLPlayer(Player p){
			plugin.logins.put(p.getUniqueId(), new Date());
			plugin.mysql.addOnlinePlayer();
			if(!plugin.mysql.checkAlias(p.getUniqueId(), p.getName())){
				plugin.mysql.addAlias(p.getUniqueId(), ", "+p.getName());
			}
	}
	
	
}
