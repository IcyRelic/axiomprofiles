package me.icyrelic.com.Listeners;

import java.util.Date;

import me.icyrelic.com.AxiomProfile;

import org.bukkit.ChatColor;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ProQuit implements Listener{
	
	AxiomProfile plugin;
	public ProQuit(AxiomProfile instance) {

		plugin = instance;

		}
	ChatColor bold = ChatColor.BOLD;
    ChatColor italic = ChatColor.ITALIC;
    ChatColor underline = ChatColor.UNDERLINE;
    ChatColor magic = ChatColor.MAGIC;
    ChatColor strike = ChatColor.STRIKETHROUGH;
    ChatColor reset = ChatColor.RESET; 
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event){
			plugin.mysql.delOnlinePlayer();
			Date logout = new Date();
			Date login = plugin.logins.get(event.getPlayer().getName().toLowerCase());
			Player p = event.getPlayer();
			Damageable d = (Damageable) p;
			plugin.mysql.updateStats(p.getUniqueId(), p.getFoodLevel()+"/20", d.getHealth()+"/20", p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ(), p.getItemInHand().getType().toString(), login, logout);
			plugin.logins.remove(event.getPlayer().getName());
		
	}
	
}
