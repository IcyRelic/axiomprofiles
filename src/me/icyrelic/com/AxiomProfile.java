package me.icyrelic.com;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import me.icyrelic.com.Commands.*;
import me.icyrelic.com.Data.ProMySQL;
import me.icyrelic.com.Listeners.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;


public class AxiomProfile extends JavaPlugin{
	
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "AxiomProfile" + ChatColor.WHITE + "] ");
	public HashMap<UUID, Date> logins = new HashMap<UUID, Date>();
	public ProMySQL mysql = new ProMySQL(this);
	public String tbl_prefix;
	public String players_tbl;
	public String information_tbl;
	
	public void onEnable() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		loadConfiguration();
		
		tbl_prefix = getConfig().getString("MySQL.Table_Prefix");
		
		players_tbl = tbl_prefix+"players";
		information_tbl = tbl_prefix+"information";
		
			
				String host = getConfig().getString("MySQL.Host");
				String port = getConfig().getString("MySQL.Port");
				String user = getConfig().getString("MySQL.Username");
				String password = getConfig().getString("MySQL.Password");
				String db = getConfig().getString("MySQL.Database");
				console.sendMessage(prefix+"Trying To Connect To Database '"+db+"'");
				
				
				
				if(mysql.connect(host, port, db, user, password)){
					console.sendMessage(prefix+"Connected To Database '"+db+"'");
				}else{
					console.sendMessage(prefix+"Failed To Connect To Database '"+db+"'");
					console.sendMessage(prefix + "Shutting Down...");
					getServer().getPluginManager().disablePlugin(this);
				}
		
		
		
		//Load Listeners and Commands		
		Bukkit.getServer().getPluginManager().registerEvents(new ProJoin(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ProQuit(this), this);
		getCommand("profile").setExecutor(new ProfileCommand(this));
	}

	public void onDisable(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		
		console.sendMessage(prefix + "Shutting Down...");
		
			console.sendMessage(prefix + "Disconnecting From Database...");
			String db = getConfig().getString("MySQL.Database");
			mysql.shutdown(db);
		
		console.sendMessage(prefix + "Disabled");
		
	}
	
	public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	
	public AxiomForum getAxiomForum() {
	    Plugin pl = getServer().getPluginManager().getPlugin("AxiomForum");
	 
	    if (pl == null || !(pl instanceof AxiomForum)) {
	        return null;
	    }
	 
	    return (AxiomForum) pl;
	}
		
	
	

}